package de.ul.mds.pprl.transform.service.internal;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class NormalizationTransformerTest {

    @Test
    public void testThrowNullString() {
        assertThrows(NullPointerException.class, () -> new NormalizationTransformer().transform(null));
    }

    @Test
    public void testNormalizer() {
        // this string contains different casings, diacritics, internal and external whitespaces
        // and code points that aren't caught by the default normalizer.
        assertEquals("foo bar", new NormalizationTransformer().transform(" Fꝏ   bár "));
    }

}
