package de.ul.mds.pprl.transform.service.internal;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class NumberTransformerTest {

    @Test
    public void testThrowNegativeDecimalPlaces() {
        assertThrows(IllegalArgumentException.class, () -> new NumberTransformer(-1));
    }

    @Test
    public void testThrowNullString() {
        assertThrows(NullPointerException.class, () -> new NumberTransformer(1).transform(null));
    }

    @Test
    public void testNumberTransformerNoDecimalPlaces() {
        NumberTransformer p = new NumberTransformer(0);

        assertEquals("2", p.transform("002.000"));
        assertEquals("2", p.transform("2.5"));
        assertEquals("3", p.transform("2.51"));
        assertEquals("4", p.transform("3.5"));
        assertEquals("-2", p.transform("-2.5"));
    }

    @Test
    public void testNumberTransformerWithDecimalPlaces() {
        NumberTransformer p = new NumberTransformer(2);

        assertEquals("2.00", p.transform("2"));
        assertEquals("2.11", p.transform("2.111"));
        assertEquals("2.50", p.transform("2.501"));
        assertEquals("-2.50", p.transform("-2.501"));
        assertEquals("2.04", p.transform("2.035"));
        assertEquals("2.02", p.transform("2.025"));
    }

}
