package de.ul.mds.pprl.transform.service.internal;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PhoneticCodeTransformerTest {

    @Test
    public void testThrowNullAlgorithm() {
        assertThrows(NullPointerException.class, () -> new PhoneticCodeTransformer(null));
    }

    @Test
    public void testThrowNullString() {
        assertThrows(NullPointerException.class, () -> new PhoneticCodeTransformer(PhoneticCodeTransformer.Algorithm.COLOGNE)
                .transform(null));
    }

    @Test
    public void testAllAlgorithms() {
        for (PhoneticCodeTransformer.Algorithm alg : PhoneticCodeTransformer.Algorithm.values()) {
            PhoneticCodeTransformer c = new PhoneticCodeTransformer(alg);
            assertNotNull(c.transform("foobar"));
        }
    }

}
