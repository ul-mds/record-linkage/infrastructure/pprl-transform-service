package de.ul.mds.pprl.transform.service.internal;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CharacterFilterTransformerTest {

    @Test
    public void testThrowNullChars() {
        assertThrows(NullPointerException.class, () -> new CharacterFilterTransformer(null));
    }

    @Test
    public void testThrowNullString() {
        assertThrows(NullPointerException.class, () -> new CharacterFilterTransformer().transform(null));
    }

    @Test
    public void testCustomSpecialChars() {
        assertEquals("bbb", new CharacterFilterTransformer("a").transform("ababab"));
    }

    @Test
    public void testDefaultSpecialChars() {
        assertEquals("foobar", new CharacterFilterTransformer().transform("foo!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~bar"));
    }

}
