package de.ul.mds.pprl.transform.service.internal;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DateTimeTransformerTest {

    private static final String VALID_FORMAT = "yyyy-MM-dd";

    private static final String INVALID_FORMAT = "f";

    @Test
    public void testThrowNullInputFormat() {
        assertThrows(NullPointerException.class, () -> new DateTimeTransformer(null, VALID_FORMAT));
    }

    @Test
    public void testThrowNullOutputFormat() {
        assertThrows(NullPointerException.class, () -> new DateTimeTransformer(VALID_FORMAT, null));
    }

    @Test
    public void testThrowInvalidInputFormat() {
        assertThrows(IllegalArgumentException.class, () -> new DateTimeTransformer(INVALID_FORMAT, VALID_FORMAT));
    }

    @Test
    public void testThrowInvalidOutputFormat() {
        assertThrows(IllegalArgumentException.class, () -> new DateTimeTransformer(VALID_FORMAT, INVALID_FORMAT));
    }

    @Test
    public void testThrowNullString() {
        assertThrows(NullPointerException.class, () -> new DateTimeTransformer("yyyy-MM-dd", "yyyyMMdd")
                .transform(null));
    }

    @Test
    public void testDateTimeTransformer() {
        DateTimeTransformer dt = new DateTimeTransformer("yyyy-MM-dd", "yyyyMMdd");
        assertEquals("20220831", dt.transform("2022-08-31"));
    }

    @Test
    public void testThrowInvalidInputValue() {
        DateTimeTransformer dt = new DateTimeTransformer("yyyy-MM-dd", "yyyyMMdd");
        assertThrows(IllegalArgumentException.class, () -> dt.transform("31.08.2022"));
    }

    @Test
    public void testThrowInvalidInputForOutputPattern() {
        DateTimeTransformer dt = new DateTimeTransformer("yyyy-MM-dd", "HH:mm:ss");
        assertThrows(IllegalArgumentException.class, () -> dt.transform("2022-08-31"));
    }
    
}
