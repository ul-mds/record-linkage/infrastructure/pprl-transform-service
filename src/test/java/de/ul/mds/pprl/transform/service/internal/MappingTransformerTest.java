package de.ul.mds.pprl.transform.service.internal;

import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class MappingTransformerTest {

    @Test
    public void testThrowNullDefaultValue() {
        assertThrows(NullPointerException.class, () -> new MappingTransformer(null));
    }

    @Test
    public void testThrowNullString() {
        assertThrows(NullPointerException.class, () -> new MappingTransformer(Map.of()).transform(null));
    }

    @Test
    public void testThrowNoDefault() {
        assertThrows(IllegalArgumentException.class, () -> new MappingTransformer(Map.of()).transform("foobar"));
    }

    @Test
    public void testReturnMapping() {
        assertEquals("f", new MappingTransformer(Map.of(
                "male", "m",
                "female", "f"
        )).transform("female"));
    }

    @Test
    public void testReturnDefaultValue() {
        assertEquals("u", new MappingTransformer(Map.of(
                "male", "m",
                "female", "f"
        ), "u").transform("divers"));
    }

    @Test
    public void testInlineMapping() {
        // o -> b, b -> a, therefore foobar should turn into fbbaar
        // b's introduced by the mapping should *not* be converted to a's
        assertEquals("fbbaar", new MappingTransformer(Map.of(
                "o", "b",
                "b", "a"
        ), true).transform("foobar"));

        // try the same with multiple characters
        assertEquals("barfoo", new MappingTransformer(Map.of(
                "foo", "bar",
                "bar", "foo"
        ), true).transform("foobar"));
    }

    @Test
    public void testInlineMappingCollision() {
        var ex = assertThrows(IllegalArgumentException.class, () -> new MappingTransformer(Map.of(
                "ob", "x",
                "ba", "y"
        ), true).transform("foobar"));

        assertTrue(ex.getMessage().contains("cannot resolve inline mapping"));
    }

}
