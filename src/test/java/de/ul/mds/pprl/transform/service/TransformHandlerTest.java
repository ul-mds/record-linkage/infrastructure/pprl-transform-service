package de.ul.mds.pprl.transform.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import de.ul.mds.pprl.transform.service.model.*;
import io.javalin.Javalin;
import org.junit.jupiter.api.Test;

import javax.management.Attribute;
import java.util.*;

import static io.javalin.testtools.JavalinTest.test;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class TransformHandlerTest {

    static final ObjectMapper MAPPER = new ObjectMapper();

    static List<String> collectRequestBodyErrorsFrom(String s) {
        try {
            JsonNode node = MAPPER.readValue(s, JsonNode.class);
            var errorMessages = new ArrayList<String>();

            for (JsonNode errorNode : node.get("REQUEST_BODY")) {
                errorMessages.add(errorNode.get("message").asText());
            }

            return errorMessages;
        } catch (JsonProcessingException e) {
            fail("response body is not valid JSON");
        }

        return List.of();
    }

    static Javalin app() {
        return new TransformApplication().app();
    }

    @Test
    public void testBadRequestOnEmptyEntityList() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityTransformRequest(
                    List.of(),
                    List.of(new AttributeTransformer("foobar", List.of(new Transformer("norm"))))
            ));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("entities is empty");
        });
    }

    @Test
    public void testBadRequestOnUnknownTransformer() {
        test(app(), (server, client) -> {
             var resp = client.post("/", new EntityTransformRequest(
                     List.of(new AttributeValueEntity("foo", Map.of("bar", "baz"))),
                     List.of(new AttributeTransformer("bar", List.of(new Transformer("foobar"))))
             ));

             assertThat(resp.code()).isEqualTo(400);
             assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("unknown transformer \"foobar\"");
        });
    }

    @Test
    public void testBadRequestOnRedefinedAttributeTransformers() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityTransformRequest(
                    List.of(new AttributeValueEntity("foo", Map.of("bar", "baz"))),
                    List.of(
                            new AttributeTransformer("bar", List.of(new Transformer("norm"))),
                            new AttributeTransformer("bar", List.of(new Transformer("norm")))
                    )
            ));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("transformers for attribute \"bar\" are redefined");
        });
    }

    @Test
    public void testBadRequestOnBadGlobalTransformerOrder() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityTransformRequest(
                    List.of(new AttributeValueEntity("foo", Map.of("bar", "baz"))),
                    List.of(),
                    List.of(new GlobalTransformer("norm", "foobar"))
            ));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("invalid order \"foobar\" for global transformer");
        });
    }

    @Test
    public void testBadRequestOnCharFilterTransformerCharactersNotAString() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityTransformRequest(
                    List.of(new AttributeValueEntity("foo", Map.of("bar", "baz"))),
                    List.of(new AttributeTransformer("bar", List.of(
                            new Transformer("charFilter", Map.of(
                                    "characters", 123
                            ))
                    )))
            ));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("character filter must be a string");
        });
    }

    @Test
    public void testBadRequestOnNoDateTimeInputFormat() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityTransformRequest(
                    List.of(new AttributeValueEntity("foo", Map.of("bar", "baz"))),
                    List.of(new AttributeTransformer("bar", List.of(
                            new Transformer("dateTime", Map.of(
                                    "outputFormat", "yyyyMMdd"
                            ))
                    )))
            ));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("no input format for date time transformer specified");
        });
    }

    @Test
    public void testBadRequestOnNoDateTimeOutputFormat() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityTransformRequest(
                    List.of(new AttributeValueEntity("foo", Map.of("bar", "baz"))),
                    List.of(new AttributeTransformer("bar", List.of(
                            new Transformer("dateTime", Map.of(
                                    "inputFormat", "yyyy-MM-dd"
                            ))
                    )))
            ));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("no output format for date time transformer specified");
        });
    }

    @Test
    public void testBadRequestOnDateTimeInputFormatNotAString() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityTransformRequest(
                    List.of(new AttributeValueEntity("foo", Map.of("bar", "baz"))),
                    List.of(new AttributeTransformer("bar", List.of(
                            new Transformer("dateTime", Map.of(
                                    "inputFormat", 123,
                                    "outputFormat", "yyyyMMdd"
                            ))
                    )))
            ));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("date time transformer input format must be a string");
        });
    }

    @Test
    public void testBadRequestOnDateTimeOutputFormatNotAString() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityTransformRequest(
                    List.of(new AttributeValueEntity("foo", Map.of("bar", "baz"))),
                    List.of(new AttributeTransformer("bar", List.of(
                            new Transformer("dateTime", Map.of(
                                    "inputFormat", "yyyy-MM-dd",
                                    "outputFormat", 123
                            ))
                    )))
            ));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("date time transformer output format must be a string");
        });
    }

    @Test
    public void testBadRequestOnNoMapping() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityTransformRequest(
                    List.of(new AttributeValueEntity("foo", Map.of("bar", "baz"))),
                    List.of(new AttributeTransformer("bar", List.of(
                            new Transformer("mapping")
                    )))
            ));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("no mapping for mapping transformer specified");
        });
    }

    @Test
    public void testBadRequestOnMappingNotAMap() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityTransformRequest(
                    List.of(new AttributeValueEntity("foo", Map.of("bar", "baz"))),
                    List.of(new AttributeTransformer("bar", List.of(
                            new Transformer("mapping", Map.of(
                                    "mapping", 123
                            ))
                    )))
            ));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("mapping must be a key-value object");
        });
    }

    @Test
    public void testBadRequestOnMappingValuesNotStrings() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityTransformRequest(
                    List.of(new AttributeValueEntity("foo", Map.of("bar", "baz"))),
                    List.of(new AttributeTransformer("bar", List.of(
                            new Transformer("mapping", Map.of(
                                    "mapping", Map.of(
                                            "foo", 123
                                    )
                            ))
                    )))
            ));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("values in mapping key-value object must be strings");
        });
    }

    @Test
    public void testBadRequestOnMappingDefaultNotAString() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityTransformRequest(
                    List.of(new AttributeValueEntity("foo", Map.of("bar", "baz"))),
                    List.of(new AttributeTransformer("bar", List.of(
                            new Transformer("mapping", Map.of(
                                    "mapping", Map.of("bar", "123"),
                                    "default", 123
                            ))
                    )))
            ));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("default value for mapping transformer must be a string");
        });
    }

    @Test
    public void testBadRequestOnMappingInlineNotABoolean() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityTransformRequest(
                    List.of(new AttributeValueEntity("foo", Map.of("bar", "baz"))),
                    List.of(new AttributeTransformer("bar", List.of(
                            new Transformer("mapping", Map.of(
                                    "mapping", Map.of("bar", "123"),
                                    "inline", 123
                            ))
                    )))
            ));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("inline value for mapping transformer must be a boolean");
        });
    }

    @Test
    public void testBadRequestOnDecimalsNotAnInteger() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityTransformRequest(
                    List.of(new AttributeValueEntity("foo", Map.of("bar", "baz"))),
                    List.of(new AttributeTransformer("bar", List.of(
                            new Transformer("number", Map.of(
                                    "decimals", "123"
                            ))
                    )))
            ));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("decimal places must be an integer");
        });
    }

    @Test
    public void testBadRequestOnNoPhoneticCodeAlgorithm() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityTransformRequest(
                    List.of(new AttributeValueEntity("foo", Map.of("bar", "baz"))),
                    List.of(new AttributeTransformer("bar", List.of(
                            new Transformer("phonetic")
                    )))
            ));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("no algorithm for phonetic code transformer specified");
        });
    }

    @Test
    public void testBadRequestOnPhoneticCodeAlgorithmNotAString() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityTransformRequest(
                    List.of(new AttributeValueEntity("foo", Map.of("bar", "baz"))),
                    List.of(new AttributeTransformer("bar", List.of(
                            new Transformer("phonetic", Map.of(
                                    "algorithm", 123
                            ))
                    )))
            ));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("algorithm must be a string");
        });
    }

    @Test
    public void testCharacterFilterDefault() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityTransformRequest(
                    List.of(
                            new AttributeValueEntity("foo1", Map.of("bar", "ba-z"))
                    ),
                    List.of(
                            new AttributeTransformer("bar", List.of(
                                    new Transformer("charFilter")
                            ))
                    )
            ));

            assertThat(resp.code()).isEqualTo(200);

            var r = MAPPER.readValue(resp.body().string(), EntityTransformResponse.class);

            assertThat(r.getEntities()).hasSize(1);
            assertThat(r.getEntities().get(0).getAttributeValuePairs().get("bar")).isEqualTo("baz");
        });
    }

    @Test
    public void testCharacterFilterCustom() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityTransformRequest(
                    List.of(
                            new AttributeValueEntity("foo1", Map.of("bar", "baäßz"))
                    ),
                    List.of(
                            new AttributeTransformer("bar", List.of(
                                    new Transformer("charFilter", Map.of(
                                            "characters", "äöüß"
                                    ))
                            ))
                    )
            ));

            assertThat(resp.code()).isEqualTo(200);

            var r = MAPPER.readValue(resp.body().string(), EntityTransformResponse.class);

            assertThat(r.getEntities()).hasSize(1);
            assertThat(r.getEntities().get(0).getAttributeValuePairs().get("bar")).isEqualTo("baz");
        });
    }

    @Test
    public void testDateTime() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityTransformRequest(
                    List.of(
                            new AttributeValueEntity("foo1", Map.of("bar", "1998-06-29"))
                    ),
                    List.of(
                            new AttributeTransformer("bar", List.of(
                                    new Transformer("dateTime", Map.of(
                                            "inputFormat", "yyyy-MM-dd",
                                            "outputFormat", "yyyyMMdd"
                                    ))
                            ))
                    )
            ));

            assertThat(resp.code()).isEqualTo(200);

            var r = MAPPER.readValue(resp.body().string(), EntityTransformResponse.class);

            assertThat(r.getEntities()).hasSize(1);
            assertThat(r.getEntities().get(0).getAttributeValuePairs().get("bar")).isEqualTo("19980629");
        });
    }

    @Test
    public void testBadRequestOnInvalidDateTimeInput() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityTransformRequest(
                    List.of(
                            new AttributeValueEntity("foo1", Map.of("bar", "baz"))
                    ),
                    List.of(
                            new AttributeTransformer("bar", List.of(
                                    new Transformer("dateTime", Map.of(
                                            "inputFormat", "yyyy-MM-dd",
                                            "outputFormat", "yyyyMMdd"
                                    ))
                            ))
                    )
            ));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("failed to transform entity with ID \"foo1\": invalid input value \"baz\" for format \"yyyy-MM-dd\"");
        });
    }

    @Test
    public void testBadRequestOnInsufficientOutputFormatInformation() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityTransformRequest(
                    List.of(
                            new AttributeValueEntity("foo1", Map.of("bar", "1998-06-29"))
                    ),
                    List.of(
                            new AttributeTransformer("bar", List.of(
                                    new Transformer("dateTime", Map.of(
                                            "inputFormat", "yyyy-MM-dd",
                                            "outputFormat", "HH:mm:ss"
                                    ))
                            ))
                    )
            ));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("failed to transform entity with ID \"foo1\": output pattern \"HH:mm:ss\" is missing information from input value \"1998-06-29\"");
        });
    }

    @Test
    public void testMapping() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityTransformRequest(
                    List.of(
                            new AttributeValueEntity("foo1", Map.of("bar", "baz"))
                    ),
                    List.of(
                            new AttributeTransformer("bar", List.of(
                                    new Transformer("mapping", Map.of(
                                            "mapping", Map.of("baz", "bat")
                                    ))
                            ))
                    )
            ));

            assertThat(resp.code()).isEqualTo(200);

            var r = MAPPER.readValue(resp.body().string(), EntityTransformResponse.class);

            assertThat(r.getEntities()).hasSize(1);
            assertThat(r.getEntities().get(0).getAttributeValuePairs().get("bar")).isEqualTo("bat");
        });
    }

    @Test
    public void testMappingWithDefault() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityTransformRequest(
                    List.of(
                            new AttributeValueEntity("foo1", Map.of("bar", "baz"))
                    ),
                    List.of(
                            new AttributeTransformer("bar", List.of(
                                    new Transformer("mapping", Map.of(
                                            "default", "bat", // default mapping should be used
                                            "mapping", Map.of("x", "y") // this won't match with "bar"
                                    ))
                            ))
                    )
            ));

            assertThat(resp.code()).isEqualTo(200);

            var r = MAPPER.readValue(resp.body().string(), EntityTransformResponse.class);

            assertThat(r.getEntities()).hasSize(1);
            assertThat(r.getEntities().get(0).getAttributeValuePairs().get("bar")).isEqualTo("bat");
        });
    }

    @Test
    public void testMappingInline() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityTransformRequest(
                    List.of(
                            new AttributeValueEntity("foo1", Map.of("bar", "baz"))
                    ),
                    List.of(
                            new AttributeTransformer("bar", List.of(
                                    new Transformer("mapping", Map.of(
                                            "inline", true,
                                            "mapping", Map.of("z", "t") // this should transform "baz" to "bat"
                                    ))
                            ))
                    )
            ));

            assertThat(resp.code()).isEqualTo(200);

            var r = MAPPER.readValue(resp.body().string(), EntityTransformResponse.class);

            assertThat(r.getEntities()).hasSize(1);
            assertThat(r.getEntities().get(0).getAttributeValuePairs().get("bar")).isEqualTo("bat");
        });
    }

    @Test
    public void testNormalize() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityTransformRequest(
                            List.of(new AttributeValueEntity("foo1", Map.of(
                                    "bar1", "bäz",
                                    "bar2", " bÀ   ar "
                            ))), List.of(
                            new AttributeTransformer("bar2", List.of(new Transformer("norm")))
                    ))
            );

            assertThat(resp.code()).isEqualTo(200);

            var r = MAPPER.readValue(resp.body().string(), EntityTransformResponse.class);

            assertThat(r.getEntities()).hasSize(1);
            assertThat(r.getEntities().get(0).getAttributeValuePairs().get("bar1")).isEqualTo("bäz");
            assertThat(r.getEntities().get(0).getAttributeValuePairs().get("bar2")).isEqualTo("ba ar");
        });
    }

    @Test
    public void testNumber() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityTransformRequest(
                    List.of(
                            new AttributeValueEntity("foo1", Map.of(
                                    "bar1", "0012.345",
                                    "bar2", "0012.345"
                            ))
                    ),
                    List.of(
                            new AttributeTransformer("bar1", List.of(new Transformer("number"))),
                            new AttributeTransformer("bar2", List.of(new Transformer("number", Map.of("decimals", 2))))
                    )
            ));

            assertThat(resp.code()).isEqualTo(200);

            var r = MAPPER.readValue(resp.body().string(), EntityTransformResponse.class);

            assertThat(r.getEntities()).hasSize(1);
            assertThat(r.getEntities().get(0).getAttributeValuePairs().get("bar1")).isEqualTo("12");
            assertThat(r.getEntities().get(0).getAttributeValuePairs().get("bar2")).isEqualTo("12.34");
        });
    }

    @Test
    public void testPhonetic() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityTransformRequest(
                    List.of(
                            new AttributeValueEntity("foo1", Map.of(
                                    "bar1", "baz",
                                    "bar2", "bass"
                            ))
                    ),
                    List.of(
                            new AttributeTransformer("bar1", List.of(
                                    new Transformer("phonetic", Map.of("algorithm", "cologne"))
                            )),
                            new AttributeTransformer("bar2", List.of(
                                    new Transformer("phonetic", Map.of("algorithm", "soundex"))
                            ))
                    )
            ));

            assertThat(resp.code()).isEqualTo(200);

            var r = MAPPER.readValue(resp.body().string(), EntityTransformResponse.class);

            assertThat(r.getEntities()).hasSize(1);
            assertThat(r.getEntities().get(0).getAttributeValuePairs().get("bar1")).isEqualTo("18");
            assertThat(r.getEntities().get(0).getAttributeValuePairs().get("bar2")).isEqualTo("B200");
        });
    }

    @Test
    public void testGlobalTransformers() {
        test(app(), (server, client) -> {
            var resp = client.post("/", new EntityTransformRequest(
                    List.of(
                            new AttributeValueEntity("foo", Map.of("bar1", "  12.345  ", "bar2", "  12.345  "))
                    ),
                    List.of(
                            new AttributeTransformer("bar1", List.of(new Transformer("number", Map.of("decimals", 2))))
                    ),
                    List.of(
                            new GlobalTransformer("norm", "before"),
                            new GlobalTransformer("charFilter", "after")
                    )
            ));

            assertThat(resp.code()).isEqualTo(200);

            var r = MAPPER.readValue(resp.body().string(), EntityTransformResponse.class);

            assertThat(r.getEntities()).hasSize(1);
            assertThat(r.getEntities().get(0).getAttributeValuePairs().get("bar1")).isEqualTo("1234");  // "  12.345  " -> "12.345" -> "12.34" -> "1234"
            assertThat(r.getEntities().get(0).getAttributeValuePairs().get("bar2")).isEqualTo("12345"); // "  12.345  " -> "12.345" -> "12345"
        });
    }

    @Test
    public void testIgnoreOnEmptyValues() {
        test(app(), (server, client) -> {
            // this request should throw an error because the datetime transformer doesn't know
            // how to deal with empty strings
            var resp = client.post("/", new EntityTransformRequest(
                    new EntityTransformConfig(EntityTransformConfig.EmptyValueHandling.IGNORE),
                    List.of(
                            new AttributeValueEntity("foo1", Map.of(
                                    "date", ""
                            ))
                    ),
                    List.of(
                            new AttributeTransformer("date", List.of(
                                    new Transformer("dateTime", Map.of(
                                            "inputFormat", "yyyy-MM-dd",
                                            "outputFormat", "yyyyMMdd"
                                    ))
                            ))
                    ),
                    List.of()
            ));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("failed to transform entity with ID \"foo1\": invalid input value \"\" for format \"yyyy-MM-dd\"");
        });
    }

    @Test
    public void testSkipOnEmptyValues() {
        test(app(), (server, client) -> {
            // this request should work although the datetime transformer cannot process empty strings.
            // the transformer should just be skipped because the input value is empty.
            var resp = client.post("/", new EntityTransformRequest(
                    new EntityTransformConfig(EntityTransformConfig.EmptyValueHandling.SKIP),
                    List.of(
                            new AttributeValueEntity("foo1", Map.of(
                                    "date", ""
                            ))
                    ),
                    List.of(
                            new AttributeTransformer("date", List.of(
                                    new Transformer("dateTime", Map.of(
                                            "inputFormat", "yyyy-MM-dd",
                                            "outputFormat", "yyyyMMdd"
                                    ))
                            ))
                    ),
                    List.of()
            ));

            assertThat(resp.code()).isEqualTo(200);

            var r = MAPPER.readValue(resp.body().string(), EntityTransformResponse.class);

            assertThat(r.getEntities()).hasSize(1);
            assertThat(r.getEntities().get(0).getAttributeValuePairs().get("date")).isEqualTo("");
        });
    }

    @Test
    public void testThrowOnEmptyValues() {
        test(app(), (server, client) -> {
            // this request should fail because it is explicitly requested to throw on empty values.
            var resp = client.post("/", new EntityTransformRequest(
                    new EntityTransformConfig(EntityTransformConfig.EmptyValueHandling.THROW),
                    List.of(
                            new AttributeValueEntity("foo1", Map.of(
                                    "date", ""
                            ))
                    ),
                    List.of(
                            new AttributeTransformer("date", List.of(
                                    new Transformer("dateTime", Map.of(
                                            "inputFormat", "yyyy-MM-dd",
                                            "outputFormat", "yyyyMMdd"
                                    ))
                            ))
                    ),
                    List.of()
            ));

            assertThat(resp.code()).isEqualTo(400);
            assertThat(collectRequestBodyErrorsFrom(resp.body().string())).contains("failed to transform entity with ID \"foo1\": value is empty before transformation using DateTimeTransformer");
        });
    }

}
