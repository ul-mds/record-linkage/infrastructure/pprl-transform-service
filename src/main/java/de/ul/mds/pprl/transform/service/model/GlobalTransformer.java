package de.ul.mds.pprl.transform.service.model;

import java.util.*;

import static java.util.Objects.requireNonNull;

/**
 * Model class that takes all attributes from {@link Transformer} and adds an "order" property.
 * This is for transformers that are applied to all attributes.
 * The order determines whether the transformation is a pre- or post-attribute processing step.
 */
public class GlobalTransformer extends Transformer {

    /**
     * Transformer order
     */
    private String order;

    /**
     * Constructs a new global transformer with no name, order and parameters.
     */
    public GlobalTransformer() {
        this("");
    }

    /**
     * Constructs a new global transformer with the specified name, no order and parameters.
     *
     * @param name transformer name
     * @throws NullPointerException if {@code name} is {@code null}
     */
    public GlobalTransformer(String name) {
        this(name, "");
    }

    /**
     * Constructs a new global transformer with the specified name, order, and no parameters.
     *
     * @param name transformer name
     * @param order transformer order
     * @throws NullPointerException if {@code name} or {@code order} is {@code null}
     */
    public GlobalTransformer(String name, String order) {
        this(name, order, new HashMap<>());
    }

    /**
     * Constructs a new global transformer with the specified name, order and parameters.
     *
     * @param name transformer name
     * @param order transformer order
     * @param parameters transformer parameters
     * @throws NullPointerException if {@code name}, {@code order} or {@code parameters} is {@code null}
     */
    public GlobalTransformer(String name, String order, Map<String, Object> parameters) {
        super(name, parameters);
        setOrder(order);
    }

    /**
     * Gets the transformer order.
     *
     * @return transformer order
     */
    public String getOrder() {
        return order;
    }

    /**
     * Sets the transformer order.
     *
     * @param order transformer order
     * @throws NullPointerException if {@code order} is {@code null}
     */
    public void setOrder(String order) {
        this.order = requireNonNull(order);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        GlobalTransformer that = (GlobalTransformer) o;
        return order.equals(that.order);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), order);
    }

    @Override
    public String toString() {
        return "GlobalTransformer{" +
                "name='" + getName() + '\'' +
                ", order='" + order + '\'' +
                ", parameters=" + getParameters() +
                '}';
    }

}
