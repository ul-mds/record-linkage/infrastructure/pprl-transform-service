package de.ul.mds.pprl.transform.service.internal;

import java.time.DateTimeException;
import java.time.format.*;
import java.time.temporal.TemporalAccessor;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;

/**
 * The date time transformer performs transformations of date time representations.
 * This can be useful, for example, if your data holds timestamps where only the date portion is of relevance.
 * You can specify a condensed output format, which only holds relevant information to avoid false positive matches, specifically in PPRL.
 * An example could look like this:
 *
 * <pre>
 *     <code>DateTimeTransformer dt = new DateTimeTransformer("yyyy-MM-dd'T'HH:mm:ss'Z'", "yyyyMMdd");</code>
 *     <code>dt.transform("2022-08-31T00:00:00Z"); // => "20220831"</code>
 * </pre>
 */
public class DateTimeTransformer implements StringTransformer {

    private final String inputPattern, outputPattern;

    /**
     * Date time input formatter.
     */
    private final DateTimeFormatter inputFormatter;

    /**
     * Date time output formatter.
     */
    private final DateTimeFormatter outputFormatter;

    /**
     * Creates a new date time transformer with the specified input and output patterns.
     *
     * @param inputPattern Date time input pattern
     * @param outputPattern Date time output pattern
     */
    public DateTimeTransformer(String inputPattern, String outputPattern) {
        this.inputPattern = inputPattern;
        this.outputPattern = outputPattern;
        this.inputFormatter = DateTimeFormatter.ofPattern(inputPattern);
        this.outputFormatter = DateTimeFormatter.ofPattern(outputPattern);
    }

    @Override
    public String transform(String input) {
        TemporalAccessor ta;

        try {
            ta = inputFormatter.parse(requireNonNull(input));
        } catch (DateTimeParseException ex)  {
            throw new IllegalArgumentException(format("invalid input value \"%s\" for format \"%s\"", input, inputPattern), ex);
        }

        try {
            return outputFormatter.format(ta);
        } catch (DateTimeException ex) {
            throw new IllegalArgumentException(format("output pattern \"%s\" is missing information from input value \"%s\"", outputPattern, input), ex);
        }
    }

}
