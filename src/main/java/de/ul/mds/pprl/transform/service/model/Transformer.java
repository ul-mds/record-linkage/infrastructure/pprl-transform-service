package de.ul.mds.pprl.transform.service.model;

import com.fasterxml.jackson.annotation.*;
import de.ul.mds.pprl.transform.service.internal.StringTransformer;

import java.util.*;

import static java.util.Objects.requireNonNull;

/**
 * Model class that represents a {@link StringTransformer StringTransformer} by name and the
 * parameters that should be applied to it.
 */
public class Transformer {

    /**
     * Transformer name
     */
    private String name;

    /**
     * Transformer parameters
     */
    private Map<String, Object> parameters;

    /**
     * Constructs a new transformer with no name and parameters.
     */
    public Transformer() {
        this("");
    }

    /**
     * Constructs a new transformer with the specified name and no parameters.
     *
     * @param name transformer name
     */
    public Transformer(String name) {
        this(name, new HashMap<>());
    }

    /**
     * Constructs a new transformer with the specified name and parameters.
     *
     * @param name transformer name
     * @param parameters transformer parameters
     * @throws NullPointerException if {@code parameters} is {@code null}
     */
    public Transformer(String name, Map<String, Object> parameters) {
        setName(name);
        setParameters(parameters);
    }

    /**
     * Gets the transformer name.
     *
     * @return transformer name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the transformer name.
     *
     * @param name transformer name
     * @throws NullPointerException if {@code name} is {@code null}
     */
    public void setName(String name) {
        this.name = requireNonNull(name);
    }

    /**
     * Gets the transformer parameters.
     *
     * @return transformer parameters
     */
    @JsonAnyGetter
    public Map<String, Object> getParameters() {
        return parameters;
    }

    /**
     * Sets the transformer parameters.
     *
     * @param parameters transformer parameters
     * @throws NullPointerException if {@code parameters} is {@code null}
     */
    public void setParameters(Map<String, Object> parameters) {
        this.parameters = requireNonNull(parameters);
    }

    /**
     * Adds a parameter to the transformer.
     *
     * @param key transformer parameter name
     * @param value transformer parameter value
     * @throws NullPointerException if either {@code key} or {@code value} is {@code null}
     */
    @JsonAnySetter
    public void putParameter(String key, Object value) {
        this.parameters.put(requireNonNull(key), requireNonNull(value));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transformer that = (Transformer) o;
        return name.equals(that.name) && parameters.equals(that.parameters);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, parameters);
    }

    @Override
    public String toString() {
        return "Transformer{" +
                "name='" + name + '\'' +
                ", parameters=" + parameters +
                '}';
    }

}
