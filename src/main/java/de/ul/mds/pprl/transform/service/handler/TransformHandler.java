package de.ul.mds.pprl.transform.service.handler;

import de.ul.mds.pprl.transform.service.internal.*;
import de.ul.mds.pprl.transform.service.model.*;
import io.javalin.http.*;
import io.javalin.validation.*;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.function.Function;

import static java.lang.String.format;
import static java.util.Map.entry;

public class TransformHandler implements Handler {

    private static <T> ValidationException newBodyValidationException(String message, T value) {
        return new ValidationException(Map.of("REQUEST_BODY", List.of(
                new ValidationError<>(message, Map.of(), value)
        )));
    }

    /**
     * Lookup map of transformer name -> transformer resolver.
     */
    final Map<String, Function<Transformer, StringTransformer>> transformerLookupMap = Map.ofEntries(
            // character filter lookups
            entry("charFilter", this::resolveCharacterFilterTransformer),
            entry("characterFilter", this::resolveCharacterFilterTransformer),
            // date time lookups
            entry("dateTime", this::resolveDateTimeTransformer),
            // mapping lookups
            entry("map", this::resolveMappingTransformer),
            entry("mapping", this::resolveMappingTransformer),
            // normalization lookups
            entry("norm", this::resolveNormalizationTransformer),
            entry("normalize", this::resolveNormalizationTransformer),
            entry("normalise", this::resolveNormalizationTransformer),
            entry("normalization", this::resolveNormalizationTransformer),
            entry("normalisation", this::resolveNormalizationTransformer),
            // number lookups
            entry("number", this::resolveNumberTransformer),
            // phonetic code lookups
            entry("phonetic", this::resolvePhoneticCodeTransformer),
            entry("phoneticCode", this::resolvePhoneticCodeTransformer)
    );

    @Override
    public void handle(@NotNull Context ctx) {
        var req = ctx.bodyValidator(EntityTransformRequest.class)
                .check(r -> !r.getEntities().isEmpty(), "entities is empty")
                .check(r -> r.getAttributeTransformers().size() + r.getGlobalTransformers().size() > 0, "attributeTransformers and globalTransformers are empty")
                .get();

        var entities = req.getEntities();
        var attrTransformers = req.getAttributeTransformers();

        // construct the mapping of attribute name ==> string transformers
        var attributeToTransformerMap = new HashMap<String, List<StringTransformer>>(attrTransformers.size());

        var globalTransformersBefore = new ArrayList<StringTransformer>(req.getGlobalTransformers().size());
        var globalTransformersAfter = new ArrayList<StringTransformer>(req.getGlobalTransformers().size());

        for (var globalTransformer : req.getGlobalTransformers()) {
            StringTransformer transformer;

            try {
                transformer = resolveTransformer(globalTransformer);
            } catch (IllegalArgumentException ex) {
                throw newBodyValidationException(ex.getMessage(), req);
            }

            String order = globalTransformer.getOrder();

            switch (order) {
                case "before" -> globalTransformersBefore.add(transformer);
                case "after" -> globalTransformersAfter.add(transformer);
                default -> throw newBodyValidationException(format("invalid order \"%s\" for global transformer", order), req);
            }
        }

        for (var attrTransformer : attrTransformers) {
            String attrName = attrTransformer.getAttributeName();
            var transformerList = new ArrayList<StringTransformer>(attrTransformers.size());
            var ret = attributeToTransformerMap.putIfAbsent(attrName, transformerList);

            if (ret != null) {
                throw newBodyValidationException(format("transformers for attribute \"%s\" are redefined", attrName), req);
            }

            var transformers = attrTransformer.getTransformers();

            if (transformers.isEmpty()) {
                throw newBodyValidationException(format("no transformers defined for attribute \"%s\"", attrName), req);
            }

            for (var transformer : transformers) {
                StringTransformer t;

                try {
                    t = resolveTransformer(transformer);
                } catch (IllegalArgumentException ex) {
                    throw newBodyValidationException(ex.getMessage(), req);
                }

                transformerList.add(t);
            }
        }

        var processedEntities = new ArrayList<AttributeValueEntity>(entities.size());

        for (var entity : entities) {
            var processedEntity = new AttributeValueEntity(entity.getId());

            for (var entry : entity.getAttributeValuePairs().entrySet()) {
                String k = entry.getKey(), v = entry.getValue();

                for (var transformer : globalTransformersBefore) {
                    // chain all global transformers which are applied before
                    v = tryTransform(req, entity, v, transformer);
                }

                // check if the attribute has transformers assigned to it
                var transformers = attributeToTransformerMap.get(k);

                if (transformers != null) {
                    // chain all transformers together
                    for (var transformer : transformers) {
                        v = tryTransform(req, entity, v, transformer);
                    }
                }

                for (var transformer : globalTransformersAfter) {
                    // chain all global transformers which are applied after
                    v = tryTransform(req, entity, v, transformer);
                }

                // put transformed attribute value on processed entity
                processedEntity.getAttributeValuePairs().put(k, v);
            }

            // append entity to list
            processedEntities.add(processedEntity);
        }

        ctx.json(new EntityTransformResponse(processedEntities));
    }

    String tryTransform(EntityTransformRequest req, AttributeValueEntity ent, String v, StringTransformer transformer) {
        try {
            if (v.isEmpty()) {
                var e = req.getConfig().getEmptyValue();

                if (e.equals(EntityTransformConfig.EmptyValueHandling.THROW)) {
                    throw new IllegalArgumentException(format("value is empty before transformation using %s",
                            transformer.getClass().getSimpleName()));
                }

                if (e.equals(EntityTransformConfig.EmptyValueHandling.SKIP)) {
                    return v;
                }
            }

            return transformer.transform(v);
        } catch (RuntimeException ex) {
            throw newBodyValidationException(format("failed to transform entity with ID \"%s\": %s",
                    ent.getId(), ex.getMessage()), req);
        }
    }

    StringTransformer resolveTransformer(Transformer transformer) {
        final String transformerName = transformer.getName();
        var transformerFunc = transformerLookupMap.get(transformerName);

        if (transformerFunc == null) {
            throw new IllegalArgumentException(format("unknown transformer \"%s\"", transformerName));
        }

        return transformerFunc.apply(transformer);
    }

    StringTransformer resolveCharacterFilterTransformer(Transformer transformer) {
        var params = transformer.getParameters();
        Object charactersObj = params.get("characters");

        // if no characters are specified, return the default transformer
        if (charactersObj == null) return new CharacterFilterTransformer();

        if (!(charactersObj instanceof String characters)) throw new IllegalArgumentException("character filter must be a string");

        // otherwise use the custom characters
        return new CharacterFilterTransformer(characters);
    }

    StringTransformer resolveDateTimeTransformer(Transformer transformer) {
        var params = transformer.getParameters();
        Object inputFormatObj = params.get("inputFormat");
        Object outputFormatObj = params.get("outputFormat");

        if (inputFormatObj == null) throw new IllegalArgumentException("no input format for date time transformer specified");
        if (outputFormatObj == null) throw new IllegalArgumentException("no output format for date time transformer specified");

        if (!(inputFormatObj instanceof String inputFormat)) throw new IllegalArgumentException("date time transformer input format must be a string");
        if (!(outputFormatObj instanceof String outputFormat)) throw new IllegalArgumentException("date time transformer output format must be a string");

        return new DateTimeTransformer(inputFormat, outputFormat);
    }

    StringTransformer resolveMappingTransformer(Transformer transformer) {
        var params = transformer.getParameters();
        Object mapping = params.get("mapping");

        if (mapping == null) throw new IllegalArgumentException("no mapping for mapping transformer specified");
        if (!(mapping instanceof Map<?, ?> mappingMap)) throw new IllegalArgumentException("mapping must be a key-value object");

        var transformerMap = new HashMap<String,String>(mappingMap.size());

        for (var entry : mappingMap.entrySet()) {
            // this is a safe cast since JSON keys are always strings
            String key = (String)entry.getKey();
            Object v = entry.getValue();

            if (!(v instanceof String value)) throw new IllegalArgumentException("values in mapping key-value object must be strings");

            transformerMap.put(key, value);
        }

        Object defaultValueObj = params.get("default");
        String defaultValue = null;

        if (defaultValueObj != null) {
            if (!(defaultValueObj instanceof String d)) throw new IllegalArgumentException("default value for mapping transformer must be a string");
            defaultValue = d;
        }

        Object inlineObj = params.get("inline");
        boolean inline = false;

        if (inlineObj != null) {
            if (!(inlineObj instanceof Boolean i)) throw new IllegalArgumentException("inline value for mapping transformer must be a boolean");
            inline = i;
        }

        return new MappingTransformer(transformerMap, defaultValue, inline);
    }

    StringTransformer resolveNormalizationTransformer(Transformer transformer) {
        return new NormalizationTransformer();
    }

    StringTransformer resolveNumberTransformer(Transformer transformer) {
        var params = transformer.getParameters();
        Object decimalPlacesObj = params.get("decimals");

        if (decimalPlacesObj == null) return new NumberTransformer();
        if (!(decimalPlacesObj instanceof Integer decimalPlaces)) throw new IllegalArgumentException("decimal places must be an integer");

        return new NumberTransformer(decimalPlaces);
    }

    StringTransformer resolvePhoneticCodeTransformer(Transformer transformer) {
        var params = transformer.getParameters();
        Object algorithmObj = params.get("algorithm");

        if (algorithmObj == null) throw new IllegalArgumentException("no algorithm for phonetic code transformer specified");
        if (!(algorithmObj instanceof String algorithm)) throw new IllegalArgumentException("algorithm must be a string");

        var alg = PhoneticCodeTransformer.Algorithm.valueOf(algorithm.toUpperCase(Locale.ROOT));
        return new PhoneticCodeTransformer(alg);
    }

}
