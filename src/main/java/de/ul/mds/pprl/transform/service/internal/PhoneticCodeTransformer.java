package de.ul.mds.pprl.transform.service.internal;

import org.apache.commons.codec.*;
import org.apache.commons.codec.language.*;

import java.util.Map;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;

/**
 * The phonetic code transformer converts a string into a phonetic code.
 * A few phonetic codes are supported, see {@link Algorithm}.
 * This is useful if you need to convert names into their phonetic equivalents.
 *
 * <pre>
 *     <code>PhoneticCodeTransformer pc = new PhoneticCodeTransformer(PhoneticCodeTransformer.Algorithm.COLOGNE);</code>
 *     <code>pc.transform("meier"); // => "67"</code>
 *     <code>pc.transform("meyer"); // => "67"</code>
 * </pre>
 */
public class PhoneticCodeTransformer implements StringTransformer {

    /**
     * List of supported phonetic codes.
     */
    public enum Algorithm {

        COLOGNE,
        METAPHONE,
        DOUBLE_METAPHONE,
        REFINED_SOUNDEX,
        SOUNDEX,

    }

    /**
     * Mapping of {@link Algorithm} to Apache Commons Codec {@link StringEncoder} instances.
     */
    private static final Map<Algorithm, StringEncoder> ALGORITHM_MAP = Map.of(
            Algorithm.COLOGNE, new ColognePhonetic(),
            Algorithm.METAPHONE, new Metaphone(),
            Algorithm.DOUBLE_METAPHONE, new DoubleMetaphone(),
            Algorithm.REFINED_SOUNDEX, new RefinedSoundex(),
            Algorithm.SOUNDEX, new Soundex()
    );

    private final StringEncoder encoder;

    /**
     * Constructs a new phonetic code transformer using the specified algorithm.
     *
     * @param alg phonetic code algorithm to use
     * @throws NullPointerException if the parameter is {@code null}
     * @throws IllegalArgumentException if the provided algorithm is unimplemented
     */
    public PhoneticCodeTransformer(Algorithm alg) {
        encoder = ALGORITHM_MAP.get(requireNonNull(alg));

        if (encoder == null) {
            throw new IllegalArgumentException(format("\"%s\" is an unimplemented phonetic code", alg));
        }
    }

    @Override
    public String transform(String input) {
        try {
            return encoder.encode(requireNonNull(input));
        } catch (EncoderException e) {
            throw new IllegalArgumentException("invalid input for phonetic code", e);
        }
    }

}
