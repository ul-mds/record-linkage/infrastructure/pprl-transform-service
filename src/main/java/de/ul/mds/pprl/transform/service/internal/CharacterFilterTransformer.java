package de.ul.mds.pprl.transform.service.internal;

import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

/**
 * The character filter transformer removes a set of characters from a string.
 * By default, all special characters in the ASCII block are filtered out, see {@link #DEFAULT_ASCII_SPECIAL_CHARS}.
 * You can select any characters to filter out.
 * This is helpful for eliminating special characters that don't provide extra information.
 *
 * <pre>
 *     <code>CharacterFilterTransformer cf = new CharacterFilterTransformer();</code>
 *     <code>cf.transform("foo-bar"); // => "foobar"</code>
 *     <code></code>
 *     <code>CharacterFilterTransformer cf = new CharacterFilterTransformer("äöüß");</code>
 *     <code>cf.transform("fööbär"); // => "fbr"</code>
 * </pre>
 */
public class CharacterFilterTransformer implements StringTransformer {

    /**
     * Special characters in the ASCII block (0-127).
     */
    public static final String DEFAULT_ASCII_SPECIAL_CHARS = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";

    /**
     * Set of code points that should be filtered out.
     */
    private final Set<Integer> codePointFilter;

    /**
     * Constructs a new character filter transformer with the characters from {@link #DEFAULT_ASCII_SPECIAL_CHARS}.
     */
    public CharacterFilterTransformer() {
        this(DEFAULT_ASCII_SPECIAL_CHARS);
    }

    /**
     * Constructs a new character filter transformer with the specified characters.
     *
     * @param specialChars string containing characters to filter out
     * @throws NullPointerException if the parameter is {@code null}
     */
    public CharacterFilterTransformer(String specialChars) {
        codePointFilter = requireNonNull(specialChars).codePoints()
                .boxed() // IntStream => Stream<Integer>
                .collect(Collectors.toSet());
    }

    @Override
    public String transform(String input) {
        StringBuilder sb = new StringBuilder(requireNonNull(input).length());

        // keep all code points that are *not* present in the set of filtered code points
        input.codePoints().filter(i -> !codePointFilter.contains(i)).forEach(sb::appendCodePoint);

        return sb.toString();
    }

}
