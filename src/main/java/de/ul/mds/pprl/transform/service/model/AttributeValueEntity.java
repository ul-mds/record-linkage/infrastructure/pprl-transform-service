package de.ul.mds.pprl.transform.service.model;

import com.fasterxml.jackson.annotation.*;

import java.util.*;

import static java.util.Objects.requireNonNull;

/**
 * Represents an entity with attribute-value pairs.
 */
public class AttributeValueEntity {

    /**
     * Entity identifier
     */
    private String id;

    /**
     * Attribute value pairs
     */
    private Map<String, String> attributeValuePairs;

    /**
     * Constructs a new entity with an empty identifier and no attributes.
     */
    public AttributeValueEntity() {
        this("");
    }

    /**
     * Constructs a new entity with the specified identifier and no attributes.
     *
     * @param id entity identifier
     * @throws NullPointerException if {@code id} is {@code null}
     */
    public AttributeValueEntity(String id) {
        this(id, new HashMap<>());
    }

    /**
     * Constructs a new entity with the specified identifier and attributes.
     *
     * @param id entity identifier
     * @param attributeValuePairs entity attributes
     * @throws NullPointerException if {@code id} or {@code attributeValuePairs} is {@code null}
     */
    public AttributeValueEntity(String id, Map<String, String> attributeValuePairs) {
        setId(id);
        setAttributeValuePairs(attributeValuePairs);
    }

    /**
     * Gets the entity identifier.
     *
     * @return entity identifier
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the entity identifier.
     *
     * @param id entity identifier
     * @throws NullPointerException if {@code id} is {@code null}
     */
    public void setId(String id) {
        this.id = requireNonNull(id);
    }

    /**
     * Gets the entity attributes.
     *
     * @return entity attributes
     */
    @JsonAnyGetter
    public Map<String, String> getAttributeValuePairs() {
        return attributeValuePairs;
    }

    /**
     * Sets the entity attributes.
     *
     * @param attributeValuePairs entity attributes
     * @throws NullPointerException if {@code attributeValuePairs} is {@code null}
     */
    public void setAttributeValuePairs(Map<String, String> attributeValuePairs) {
        this.attributeValuePairs = requireNonNull(attributeValuePairs);
    }

    /**
     * Adds an attribute-value pair to the entity.
     *
     * @param attribute entity attribute name
     * @param value entity attribute value
     * @throws NullPointerException if {@code attribute} or {@code value} is {@code null}
     */
    @JsonAnySetter
    public void putAttributeValuePair(String attribute, String value) {
        this.attributeValuePairs.put(requireNonNull(attribute), requireNonNull(value));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AttributeValueEntity that = (AttributeValueEntity) o;
        return id.equals(that.id) && attributeValuePairs.equals(that.attributeValuePairs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, attributeValuePairs);
    }

    @Override
    public String toString() {
        return "AttributeValueEntity{" +
                "id='" + id + '\'' +
                ", attributeValuePairs=" + attributeValuePairs +
                '}';
    }

}
