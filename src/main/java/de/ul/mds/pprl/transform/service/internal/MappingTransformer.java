package de.ul.mds.pprl.transform.service.internal;

import java.util.*;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;

/**
 * The mapping transformer replaces input strings with different values according to a mapping.
 * This is helpful when you want to replace long strings with shorter abbreviations or codes.
 * A simple example are genders, which are often truncated to a single character.
 *
 * <pre>
 *     <code>MappingTransformer mt = new MappingTransformer(Map.of("male", "m", "female", "f"), "u");</code>
 *     <code>mt.transform("male"); // => "m"</code>
 *     <code>mt.transform("female"); // => "f"</code>
 *     <code>mt.transform("foobar"); // => "u"</code>
 * </pre>
 *
 * Omitting the default value parameter leads to the transformer throwing an error if a value is passed in for which there is no mapping.
 *
 * <pre>
 *     <code>MappingTransformer mt = new MappingTransformer(Map.of("male", "m", "female", "f"));</code>
 *     <code>mt.transform("foobar"); // => IllegalArgumentException</code>
 * </pre>
 */
public class MappingTransformer implements StringTransformer {

    /**
     * Mapping of input to output values.
     */
    private final Map<String, String> transformMap;

    /**
     * Default value if an input value doesn't have a mapping in {@link #transformMap}. Can be {@code null}.
     */
    private final String defaultValue;

    /**
     * Whether or not mappings should be applied inline.
     */
    private final boolean inline;

    /**
     * Constructs a new mapping transformer with the specified mapping and no default value.
     *
     * @param transformMap mapping of input to output values
     * @throws NullPointerException if the mapping is {@code null}
     */
    public MappingTransformer(Map<String, String> transformMap) {
        this(transformMap, null);
    }

    /**
     * Constructs a new mapping transformer with the specified mapping and default value.
     *
     * @param transformMap mapping of input to output values
     * @param defaultValue default value if an input value isn't present in the mapping, or {@code null}
     * @throws NullPointerException if the mapping is {@code null}
     */
    public MappingTransformer(Map<String, String> transformMap, String defaultValue) {
        this(transformMap, defaultValue, false);
    }

    /**
     * Constructs a new mapping transformer with the specified mapping.
     * If {@code inline} is true, then the mappings will be applied within the string to transform.
     *
     * @param transformMap mapping of input to output values
     * @param inline whether to apply the mapping within the string to transform
     * @throws NullPointerException if the mapping is {@code null}
     */
    public MappingTransformer(Map<String, String> transformMap, boolean inline) {
        this(transformMap, null, inline);
    }

    /**
     * Constructs a new mapping transformer with the specified mapping and default value.
     * If {@code inline} is true, then the mappings will be applied within the string to transform.
     *
     * @param transformMap mapping of input to output values
     * @param defaultValue default value if an input value isn't present in the mapping, or {@code null}
     * @param inline whether to apply the mapping within the string to transform
     * @throws NullPointerException if the mapping is {@code null}
     */
    public MappingTransformer(Map<String, String> transformMap, String defaultValue, boolean inline) {
        this.transformMap = requireNonNull(transformMap);
        this.defaultValue = defaultValue;
        this.inline = inline;
    }

    @Override
    public String transform(String input) {
        String output = "";

        if (inline) {
            var pendingReplacements = new ArrayList<Replacement>();
            // track indices that are affected by a replacement. unset bit means that the character at that
            // index will not be replaced. set bit means that the character at that index will be replaced.
            // this can be used to check if there are any overlaps in linear time.
            var overlapMask = new BitSet(input.length());

            for (var mapEntry : transformMap.entrySet()) {
                var mapKey = mapEntry.getKey();
                var mapVal = mapEntry.getValue();
                var l = mapKey.length();

                for (int i = input.indexOf(mapKey); i >= 0; i = input.indexOf(mapKey, i+1)) {
                    // check that all bits in this range are unset. if any bit is set, this means that a
                    // previous pending replacement has already claimed the characters in this range.
                    if (!overlapMask.get(i, i+l).isEmpty()) {
                        throw new IllegalArgumentException(format("cannot resolve inline mapping: " +
                                        "replacement of \"%s\" with \"%s\" at index %d overlaps",
                                mapKey, mapVal, i));
                    }

                    pendingReplacements.add(new Replacement(i, mapKey, mapVal));
                    overlapMask.set(i, i+l);
                }
            }

            // if there are no replacements to be made, we can return early
            if (pendingReplacements.isEmpty()) return input;

            // sort by index because collide() expects first param to have a lower (or eq) index than second
            pendingReplacements.sort(Comparator.comparingInt(Replacement::index));

            // compute the final string length (optimize string builder initial capacity)
            int finalLength = input.length();
            for (Replacement r : pendingReplacements) finalLength += r.lenDiff();
            StringBuilder sb = new StringBuilder(finalLength);

            int i = 0;

            for (var repl : pendingReplacements) {
                sb.append(input, i, repl.index());
                sb.append(repl.value());

                i = repl.index() + repl.key().length();
            }

            sb.append(input.substring(i));

            return sb.toString();
        } else {
            output = transformMap.getOrDefault(requireNonNull(input), defaultValue);

            if (output == null) {
                throw new IllegalArgumentException(format("no mapping for value \"%s\", or no default value present", input));
            }
        }

        return output;
    }

    record Replacement(int index, String key, String value, int lenDiff) {

        Replacement(int index, String key, String value) {
            this(index, key, value, value.length() - key.length());
        }

    }

}
