package de.ul.mds.pprl.transform.service.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

public class EntityTransformConfig {

    public enum EmptyValueHandling {

        @JsonProperty("ignore")
        IGNORE,

        @JsonProperty("throw")
        THROW,

        @JsonProperty("skip")
        SKIP

    }

    private EmptyValueHandling emptyValue;

    public EntityTransformConfig() {
        this(EmptyValueHandling.IGNORE);
    }

    public EntityTransformConfig(EmptyValueHandling emptyValue) {
        setEmptyValue(emptyValue);
    }

    public EmptyValueHandling getEmptyValue() {
        return emptyValue;
    }

    public void setEmptyValue(EmptyValueHandling emptyValue) {
        this.emptyValue = requireNonNull(emptyValue);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EntityTransformConfig that = (EntityTransformConfig) o;
        return emptyValue == that.emptyValue;
    }

    @Override
    public int hashCode() {
        return Objects.hash(emptyValue);
    }

    @Override
    public String toString() {
        return "EntityTransformConfig{" +
                "emptyValue=" + emptyValue +
                '}';
    }

}
