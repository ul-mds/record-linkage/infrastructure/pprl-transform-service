package de.ul.mds.pprl.transform.service;

import de.ul.mds.pprl.transform.service.handler.TransformHandler;
import io.javalin.Javalin;

public class TransformApplication {

    private final Javalin app;

    public TransformApplication() {
        app = Javalin.create()
                .post("/", new TransformHandler());
    }

    public Javalin app() {
        return app;
    }

    public static void main(String[] args) {
        TransformApplication srv;

        srv = new TransformApplication();
        srv.app().start(8080);
    }

}
