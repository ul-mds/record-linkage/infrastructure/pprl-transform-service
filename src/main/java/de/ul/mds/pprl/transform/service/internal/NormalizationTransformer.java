package de.ul.mds.pprl.transform.service.internal;

import java.text.Normalizer;
import java.util.*;

import static java.lang.String.format;
import static java.util.Map.entry;
import static java.util.Objects.requireNonNull;

/**
 * The normalization transformer performs a series of common normalization operations on a string.
 * In order, these are the actions that it takes.
 *
 * <ul>
 *     <li>replaces common special characters and ligatures in Latin alphabets (e.g. ä => ae, œ => oe)</li>
 *     <li>performs Unicode normalization according to the {@link Normalizer.Form#NFKD NFKD} form, separating diacritics from characters</li>
 *     <li>removes all non-ASCII characters</li>
 *     <li>converts to lowercase</li>
 *     <li>trims internal whitespaces to a single space and removes external whitespaces</li>
 * </ul>
 *
 * The techniques are based on <a href="https://dzone.com/articles/proper-strings-normalization-for-comparison-purpos">this brilliant blog article</a>.
 */
public class NormalizationTransformer implements StringTransformer {

    /**
     * Converts a string into a code point.
     * The string must only consist of a single code point.
     *
     * @param s string to convert
     * @return code point that represents the string
     * @throws IllegalArgumentException if the string is not composed of one code point
     */
    private static int toCodePoint(String s) {
        final int cpc = s.codePointCount(0, s.length());

        if (cpc != 1) {
            throw new IllegalArgumentException(format("string \"%s\" must have exactly one codepoint, got %d", s, cpc));
        }

        return s.codePointAt(0);
    }

    /**
     * This map contains replacements that aren't covered by the default Java normalizer.
     * Taken mostly from the <a href="https://en.wikipedia.org/wiki/Ligature_(writing)#Computer_typesetting">Computer Typesetting article on Wikipedia</a>.
     */
    private static final Map<Integer, String> CHARACTER_REPLACEMENT_MAP = Map.ofEntries(
            entry(toCodePoint("ä"), "ae"),
            entry(toCodePoint("ö"), "oe"),
            entry(toCodePoint("ü"), "ue"),
            entry(toCodePoint("ß"), "ss"),
            entry(toCodePoint("Ä"), "AE"),
            entry(toCodePoint("Ö"), "OE"),
            entry(toCodePoint("Ü"), "UE"),
            entry(toCodePoint("ẞ"), "SS"),
            entry(toCodePoint("Ꜳ"), "AA"),
            entry(toCodePoint("ꜳ"), "aa"),
            entry(toCodePoint("Æ"), "AE"),
            entry(toCodePoint("æ"), "ae"),
            entry(toCodePoint("Ꜵ"), "AO"),
            entry(toCodePoint("ꜵ"), "ao"),
            entry(toCodePoint("Ꜷ"), "AU"),
            entry(toCodePoint("ꜷ"), "au"),
            entry(toCodePoint("Ꜹ"), "AV"),
            entry(toCodePoint("ꜹ"), "av"),
            entry(toCodePoint("Ꜻ"), "AV"),
            entry(toCodePoint("ꜻ"), "av"),
            entry(toCodePoint("Ꜽ"), "AY"),
            entry(toCodePoint("ꜽ"), "ay"),
            entry(toCodePoint("\uD83D\uDE70"), "ET"),
            entry(toCodePoint("ﬀ"), "ff"),
            entry(toCodePoint("ﬃ"), "ffi"),
            entry(toCodePoint("ﬄ"), "ffl"),
            entry(toCodePoint("ﬁ"), "fi"),
            entry(toCodePoint("ﬂ"), "fl"),
            entry(toCodePoint("Ƕ"), "Hv"),
            entry(toCodePoint("ƕ"), "hv"),
            entry(toCodePoint("℔"), "Ib"),
            entry(toCodePoint("Ỻ"), "IL"),
            entry(toCodePoint("ỻ"), "il"),
            entry(toCodePoint("Œ"), "OE"),
            entry(toCodePoint("œ"), "oe"),
            entry(toCodePoint("Ꝏ"), "OO"),
            entry(toCodePoint("ꝏ"), "oo"),
            entry(toCodePoint("ﬆ"), "st"),
            entry(toCodePoint("Ꜩ"), "TZ"),
            entry(toCodePoint("ꜩ"), "tz"),
            entry(toCodePoint("ᵫ"), "ue"),
            entry(toCodePoint("ꭣ"), "uo"),
            entry(toCodePoint("Ꝡ"), "VY"),
            entry(toCodePoint("ꝡ"), "vy")
    );

    /**
     * Performs replacements in an input string according to the mapping provided by {@link #CHARACTER_REPLACEMENT_MAP}.
     * Characters that are not part of the mapping are left as-is.
     *
     * @param input input string to perform replacements on
     * @return string with replaced code points
     */
    private static String performReplacements(String input) {
        StringBuilder sb = new StringBuilder(input.length());

        input.codePoints().forEach(cp -> {
            String rep = CHARACTER_REPLACEMENT_MAP.get(cp);

            if (rep == null) {
                // leave as-is
                sb.appendCodePoint(cp);
            } else {
                // insert replacement character
                sb.append(rep);
            }
        });

        return sb.toString();
    }

    @Override
    public String transform(String input) {
        // replace all ligatures that aren't covered by the standard normalizer
        String output = performReplacements(requireNonNull(input));
        // perform unicode normalization
        output = Normalizer.normalize(output, Normalizer.Form.NFKD);
        // replace all non-ascii characters
        output = output.replaceAll("[^\\p{ASCII}]", "");
        // convert to lowercase
        output = output.toLowerCase(Locale.ROOT);
        // replace internal whitespaces
        output = output.replaceAll("\\s{2,}", " ");
        // trim external whitespaces
        return output.trim();
    }

}
