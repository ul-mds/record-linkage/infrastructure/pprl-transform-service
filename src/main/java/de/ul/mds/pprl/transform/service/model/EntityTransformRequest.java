package de.ul.mds.pprl.transform.service.model;

import java.util.*;

import static java.util.Objects.requireNonNull;

public class EntityTransformRequest {

    private EntityTransformConfig config;

    /**
     * List of entities
     */
    private List<AttributeValueEntity> entities;

    /**
     * List of attribute transformers
     */
    private List<AttributeTransformer> attributeTransformers;

    /**
     * List of global transformers
     */
    private List<GlobalTransformer> globalTransformers;

    /**
     * Constructs a new entity transform request with an empty list for entities, attribute transformers and
     * global transformers.
     */
    public EntityTransformRequest() {
        this(new ArrayList<>(), new ArrayList<>());
    }

    /**
     * Constructs a new entity transform request with the specified list of entities and attribute transformers, and
     * an empty list for global transformers.
     *
     * @param entities list of entities
     * @param attributeTransformers list of attribute transformers
     * @throws NullPointerException if {@code entities} or {@code attributeTransformers} is {@code null}
     */
    public EntityTransformRequest(List<AttributeValueEntity> entities, List<AttributeTransformer> attributeTransformers) {
        this(entities, attributeTransformers, new ArrayList<>());
    }

    /**
     * Constructs a new entity transform request with the specified list of entities, attribute transformers and
     * global transformers.
     *
     * @param entities list of entities
     * @param attributeTransformers list of attribute transformers
     * @param globalTransformers list of global transformers
     * @throws NullPointerException if {@code entities}, {@code attributeTransformers} or {@code globalTransformers} is {@code null}
     */
    public EntityTransformRequest(List<AttributeValueEntity> entities, List<AttributeTransformer> attributeTransformers, List<GlobalTransformer> globalTransformers) {
        this(new EntityTransformConfig(), entities, attributeTransformers, globalTransformers);
    }

    public EntityTransformRequest(EntityTransformConfig config, List<AttributeValueEntity> entities, List<AttributeTransformer> attributeTransformers, List<GlobalTransformer> globalTransformers) {
        setConfig(config);
        setEntities(entities);
        setAttributeTransformers(attributeTransformers);
        setGlobalTransformers(globalTransformers);
    }

    public EntityTransformConfig getConfig() {
        return config;
    }

    public void setConfig(EntityTransformConfig config) {
        this.config = requireNonNull(config);
    }

    /**
     * Gets the list of entities.
     *
     * @return list of entities
     */
    public List<AttributeValueEntity> getEntities() {
        return entities;
    }

    /**
     * Sets the list of entities.
     *
     * @param entities list of entities
     * @throws NullPointerException if {@code entities} is {@code null}
     */
    public void setEntities(List<AttributeValueEntity> entities) {
        this.entities = requireNonNull(entities);
    }

    /**
     * Gets the list of attribute transformers.
     *
     * @return list of attribute transformers
     */
    public List<AttributeTransformer> getAttributeTransformers() {
        return attributeTransformers;
    }

    /**
     * Sets the list of attribute transformers.
     *
     * @param attributeTransformers list of attribute transformers
     * @throws NullPointerException if {@code attributeTransformers} is {@code null}
     */
    public void setAttributeTransformers(List<AttributeTransformer> attributeTransformers) {
        this.attributeTransformers = requireNonNull(attributeTransformers);
    }

    /**
     * Gets the list of global transformers.
     *
     * @return list of global transformers
     */
    public List<GlobalTransformer> getGlobalTransformers() {
        return globalTransformers;
    }

    /**
     * Sets the list of global transformers.
     *
     * @param globalTransformers list of global transformers
     * @throws NullPointerException if {@code globalTransformers} is {@code null}
     */
    public void setGlobalTransformers(List<GlobalTransformer> globalTransformers) {
        this.globalTransformers = requireNonNull(globalTransformers);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EntityTransformRequest that = (EntityTransformRequest) o;
        return Objects.equals(config, that.config) && Objects.equals(entities, that.entities) && Objects.equals(attributeTransformers, that.attributeTransformers) && Objects.equals(globalTransformers, that.globalTransformers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(config, entities, attributeTransformers, globalTransformers);
    }

    @Override
    public String toString() {
        return "EntityTransformRequest{" +
                "config=" + config +
                ", entities=" + entities +
                ", attributeTransformers=" + attributeTransformers +
                ", globalTransformers=" + globalTransformers +
                '}';
    }

}
