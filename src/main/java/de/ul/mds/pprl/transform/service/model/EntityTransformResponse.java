package de.ul.mds.pprl.transform.service.model;

import java.util.*;

import static java.util.Objects.requireNonNull;

/**
 * Represents a response to a {@link EntityTransformRequest}.
 */
public class EntityTransformResponse {

    /**
     * List of transformed entities
     */
    private List<AttributeValueEntity> entities;

    /**
     * Constructs a new entity transform response with an empty list of transformed entities.
     */
    public EntityTransformResponse() {
        this(new ArrayList<>());
    }

    /**
     * Constructs a new entity transform response with the specified list of transformed entities.
     *
     * @param entities list of transformed entities
     * @throws NullPointerException if {@code entities} is {@code null}
     */
    public EntityTransformResponse(List<AttributeValueEntity> entities) {
        setEntities(entities);
    }

    /**
     * Gets the list of transformed entities.
     *
     * @return list of transformed entities
     */
    public List<AttributeValueEntity> getEntities() {
        return entities;
    }

    /**
     * Sets the list of transformed entities.
     *
     * @param entities list of transformed entities
     * @throws NullPointerException if {@code entities} is {@code null}
     */
    public void setEntities(List<AttributeValueEntity> entities) {
        this.entities = requireNonNull(entities);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EntityTransformResponse that = (EntityTransformResponse) o;
        return entities.equals(that.entities);
    }

    @Override
    public int hashCode() {
        return Objects.hash(entities);
    }

    @Override
    public String toString() {
        return "EntityTransformResponse{" +
                "entities=" + entities +
                '}';
    }

}
