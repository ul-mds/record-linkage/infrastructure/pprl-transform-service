package de.ul.mds.pprl.transform.service.internal;

/**
 * {@link StringTransformer} is an interface that defines the {@link StringTransformer#transform(String) transform} function.
 * Its purpose is to apply a number of transformations to a string, returning the modified string.
 */
public interface StringTransformer {

    /**
     * Transforms an input string into an output string.
     * The set of operations performed on the string depends on the implementation.
     *
     * @param input string to transform
     * @return transformed string
     * @throws NullPointerException if the input string is {@code null}
     * @throws IllegalArgumentException if the transformer finds that the input is not valid
     */
    String transform(String input);

}
