package de.ul.mds.pprl.transform.service.model;

import java.util.*;

import static java.util.Objects.requireNonNull;

/**
 * Represents a list of {@link de.ul.mds.pprl.transform.service.model.Transformer Transformer} that are tied to
 * a certain attribute.
 */
public class AttributeTransformer {

    /**
     * Attribute name
     */
    private String attributeName;

    /**
     * Attribute transformers
     */
    private List<Transformer> transformers;

    /**
     * Creates a new attribute transformer with an empty attribute name and an empty transformer list.
     */
    public AttributeTransformer() {
        this("");
    }

    /**
     * Creates a new attribute transformer with the specified attribute name and an empty transformer list.
     *
     * @param attributeName attribute name
     * @throws NullPointerException if {@code attributeName} is {@code null}
     */
    public AttributeTransformer(String attributeName) {
        this(attributeName, new ArrayList<>());
    }

    /**
     * Creates a new attribute transformer with the specified attribute name and transformer list.
     *
     * @param attributeName attribute name
     * @param transformers attribute transformer list
     * @throws NullPointerException if {@code attributeName} or {@code transformers} is {@code null}
     */
    public AttributeTransformer(String attributeName, List<Transformer> transformers) {
        setAttributeName(attributeName);
        setTransformers(transformers);
    }

    /**
     * Gets the attribute name.
     *
     * @return attribute name
     */
    public String getAttributeName() {
        return attributeName;
    }

    /**
     * Sets the attribute name.
     *
     * @param attributeName attribute name
     * @throws NullPointerException if {@code attributeName} is {@code null}
     */
    public void setAttributeName(String attributeName) {
        this.attributeName = requireNonNull(attributeName);
    }

    /**
     * Gets the list of attribute transformers.
     *
     * @return list of attribute transformers
     */
    public List<Transformer> getTransformers() {
        return transformers;
    }

    /**
     * Sets the list of attribute transformers.
     *
     * @param transformers list of attribute transformers
     * @throws NullPointerException if {@code transformers} is {@code null}
     */
    public void setTransformers(List<Transformer> transformers) {
        this.transformers = requireNonNull(transformers);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AttributeTransformer that = (AttributeTransformer) o;
        return attributeName.equals(that.attributeName) && transformers.equals(that.transformers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(attributeName, transformers);
    }

    @Override
    public String toString() {
        return "AttributeTransformer{" +
                "attributeName='" + attributeName + '\'' +
                ", transformers=" + transformers +
                '}';
    }

}
