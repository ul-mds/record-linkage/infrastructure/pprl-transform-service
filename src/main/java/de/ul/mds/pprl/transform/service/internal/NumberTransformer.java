package de.ul.mds.pprl.transform.service.internal;

import java.math.*;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;

/**
 * The number transformer processes string representations of numbers.
 * When receiving a string, the transformer does the following:
 *
 * <ul>
 *     <li>attempts to parse the string into a floating point number</li>
 *     <li>rounds it to the specified amount of decimal places</li>
 *     <li>returns the (more compact) string representation</li>
 * </ul>
 *
 * Rounding is performed using the "half even" rule.
 * In case of a tie, a number is rounded to the nearest even number.
 * This helps to get rid of leading and trailing zeroes, as well as making the amount of decimal places uniform.
 *
 * <pre>
 *     <code>NumberTransformer n = new NumberTransformer();</code>
 *     <code>n.transform("12.345"); // => "12"</code>
 *     <code>n.transform("24.5"); // => "24"</code>
 *     <code></code>
 *     <code>NumberTransformer n = new NumberTransformer(2);</code>
 *     <code>n.transform("7.270"); // => "7.27"</code>
 *     <code>n.transform("3"); // => "3.00"</code>
 * </pre>
 */
public class NumberTransformer implements StringTransformer {

    /**
     * Amount of decimal places to keep
     */
    private final int decimalPlaces;

    /**
     * Constructs a new number transformer.
     * The amount of decimal places is set to zero.
     */
    public NumberTransformer() {
        this(0);
    }

    /**
     * Constructs a new number transformer with the specified amount of decimal places.
     *
     * @param decimalPlaces amount of decimal places to keep
     * @throws IllegalArgumentException if the parameter is less than zero
     */
    public NumberTransformer(int decimalPlaces) {
        if (decimalPlaces < 0) {
            throw new IllegalArgumentException(format("amount of significant figures cannot be negative, is %d", decimalPlaces));
        }

        this.decimalPlaces = decimalPlaces;
    }

    @Override
    public String transform(String input) {
        BigDecimal bd;

        try {
            bd = new BigDecimal(requireNonNull(input));
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("invalid input, not a floating point number", ex);
        }

        return bd.setScale(decimalPlaces, RoundingMode.HALF_EVEN).toPlainString();
    }

}
